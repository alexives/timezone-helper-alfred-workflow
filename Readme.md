# Timerzone Helper Workflow for Alfred 4

I wanted to know what time it was in my teammates timezones and calculate meeting times for them when trying to schedule things. This takes
a given time and converts it to the offsets you've specified.

![screenshot](/screenshot.png)

## Installation

- [Download the latest artifact](https://gitlab.com/alexives/timezone-helper-alfred-workflow/-/jobs/artifacts/master/download?job=build&file=Timezone-Helper.alfredworkflow)
- Open the alfredworkflow file in Alfred

## Usage

Show all of the configured timezones in your current time `timez`:

![result of timez command](/screenshots/default_times.png)

Show all configured timezones in a supplied time `timez <time>`:

![result of timez 10p](/screenshots/time_calc.png)

Filter out the timezones to show a specific one calculated `timez <filter> <time>:

![result of timez central 10p](/screenshots/time_calc_filter.png)

Add a new timezone with `timez update <zone name>, <utc offset>`:

![result of timez update new, 10](/screenshots/add_time.png)

Update an existing timezone's offset with the same command:

![result of timez update NA Central, -6](/screenshots/update_time.png)

## Development

### Setup
- Install Ruby (Tests run in 2.6.3)
- Set up dependencies:
  ```sh
  gem install bundler
  bundle install
  ```

### Running Tests

```
bundle exec rspec
```

### Building the package

```
bundle exec rake package
```
