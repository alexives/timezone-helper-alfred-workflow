require 'json'
require 'fileutils'

def get_zones(file)
  return {
    "NA Eastern Time" => -5,
    "NA Central Time" => -6,
    "NA Pacific Time" => -8
  } unless File.exist?(file)

  JSON.parse(File.read(file))
end

def format_zones_json(zones, argument, time=Time.now)
  filters = []
  time_a = []
  last_time = ''
  if argument && !argument.empty?
    argument.split(' ').each do |arg|
      if arg.match(/(\d+):?(\d+)?(am?|pm?)?/i)
        filters.push(last_time) unless time_a.empty?
        last_time = arg
        time_a = arg.match(/(\d+):?(\d+)?(am?|pm?)?/i).to_a
        time_a[1] = time_a[1].to_i + 12 if time_a[3]&.match(/pm?/i)
        time_a[2] = 0 if time_a[2].nil?
      else
        filters.push(arg)
      end
    end
  end

  # throwaway way to make a new date object in local time that we can convert to UTC based optionally with minutes/hours
  time = Time.new(2000, 'jan', 1, (time_a[1] || time.hour), (time_a[2] || time.min), 0, time.strftime('%:z'))
  time_string = time.strftime('%H:%M')
  time = time.utc

  items = zones.select do |member, _offset|
    # All filters must match against the zone for it to pass
    filters.reject do |filter|
      member.match(/#{filter}/im)
    end.empty?
  end.map do |member, offset|
    member_time = (time + offset * 60 * 60)
    {
      title: "#{member_time.strftime('%H:%M')} - #{member}",
      subtitle: "At #{time_string} here it is #{member_time.strftime('%H:%M')} for #{member} (UTC#{offset})",
      icon: {
        path: "images/#{member_time.strftime('%H').to_i % 12}.png"
      }
    }
  end

  response = { 'items' => items }
  response.to_json
end

def output_zones(argument)
  format_zones_json(get_zones('zones.json'), argument)
end
