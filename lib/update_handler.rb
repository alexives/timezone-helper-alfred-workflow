require 'json'
require_relative './view.rb'

def handle_update(arguments)
  name, offset, remove = arguments.split(',').map(&:strip)

  zones = get_zones('zones.json')
  if remove == 'true'
    zones.delete(name)
  else
    zones[name] = offset.to_i
  end

  File.write('zones.json', zones.to_json)
end
