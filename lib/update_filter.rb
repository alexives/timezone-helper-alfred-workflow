require 'json'
require_relative './view.rb'

def get_new_zone(name, offset)
  if offset&.sub('+', '')&.match(/^-?\d+$/) && offset.to_i <= 14 && offset.to_i >= -12
    {
      title: "Add/update zone #{name}/#{offset}",
      subtitle: "Add/update zone '#{name}' with offset #{offset}, ⌘+⏎ to remove",
      arg: "#{name}, #{offset}",
      valid: true,
      mods: {
        cmd: {
          subtitle: "Remove zone named #{name}",
          arg: "#{name}, 0, true"
        }
      }
    }
  elsif offset
    {
      title: "ERROR: Add/update zone #{name}/#{offset}",
      subtitle: "Invalid offset, must be an integer between -12 and +14 but is #{offset}",
      arg: "#{name}, #{offset}",
      valid: false,
      mods: {
        cmd: {
          valid: true,
          title: "Remove zone #{name}/#{offset}",
          subtitle: "Remove zone named #{name}",
          arg: "#{name}, 0, true"
        }
      }
    }
  else
    {
      title: "Add/update zone #{name}/#{offset}",
      subtitle: "Add/update zone '#{name}'",
      autocomplete: "#{name}, ",
      valid: false,
      mods: {
        cmd: {
          valid: true,
          title: "Remove zone #{name}/#{offset}",
          subtitle: "Remove zone named #{name}",
          arg: "#{name}, 0, true"
        }
      }
    }
  end
end

def format_update_json(zones, arguments)
  name, offset = (arguments || '').split(',').map(&:strip)
  selected = zones.select do |zone_name, _zone_offset|
    zone_name.match(/.*#{name}.*/) && zone_name != name
  end

  items = []
  items = [get_new_zone(name, offset)] if name
  items += selected.map do |zone_name, zone_offset|
    {
      title: "Update zone #{zone_name}/#{zone_offset}",
      subtitle: "Update zone '#{zone_name}' with offset #{zone_offset}",
      autocomplete: "#{zone_name}, #{zone_offset}",
      valid: false
    }
  end

  { items: items }.to_json
end

def add_or_update_timezone(argument)
  format_update_json(get_zones('zones.json'), argument)
end
