require_relative '../lib/view.rb'

describe 'get_zones' do
  it 'reads the provided file from disk' do
    expect(get_zones('spec/fixtures/zones.json').keys.length).to eq(5)
  end

  it 'uses defaults if json file does not exist' do
    get_zones('test.json')
    expect(get_zones('test.json').keys.length).to eq(3)
  end
end

describe 'format_zones_json' do
  let(:frozen_time) { Time.new(2016, 'jan', 6, 17, 17, 0, '+00:00') }
  let(:zones) do
    {
      'First' => -1,
      'Second' => 10,
      'Third' => -5,
      '10' => 10
    }
  end

  it 'returns the current time in the various zones if nothing is supplied' do
    formatted = JSON.parse(format_zones_json(zones, nil, frozen_time))['items']
    expect(formatted.length).to eq(4)
    expect(formatted[0]['title']).to match(/^16:17 - .*$/)
    expect(formatted[1]['title']).to match(/^03:17 - .*$/)
    expect(formatted[2]['title']).to match(/^12:17 - .*$/)
    expect(formatted[3]['title']).to match(/^03:17 - .*$/)
  end

  it 'returns the current time, but with hours changed if given hours' do
    formatted = JSON.parse(format_zones_json(zones, '10', frozen_time))['items']
    expect(formatted.length).to eq(4)
    expect(formatted[0]['title']).to match(/^09:00 - .*$/)
    expect(formatted[1]['title']).to match(/^20:00 - .*$/)
    expect(formatted[2]['title']).to match(/^05:00 - .*$/)
    expect(formatted[3]['title']).to match(/^20:00 - .*$/)
  end

  it 'returns the time provided in the requested time zones' do
    formatted = JSON.parse(format_zones_json(zones, '10:30', frozen_time))['items']
    expect(formatted.length).to eq(4)
    expect(formatted[0]['title']).to match(/^09:30 - .*$/)
    expect(formatted[1]['title']).to match(/^20:30 - .*$/)
    expect(formatted[2]['title']).to match(/^05:30 - .*$/)
    expect(formatted[3]['title']).to match(/^20:30 - .*$/)
  end

  it 'filters by title if provided a string instead of a time' do
    formatted = JSON.parse(format_zones_json(zones, 'ir', frozen_time))['items']
    expect(formatted.length).to eq(2)
    expect(formatted[0]['title']).to match(/^.* - First$/)
    expect(formatted[1]['title']).to match(/^.* - Third$/)
  end

  it 'filters to a single match' do
    formatted = JSON.parse(format_zones_json(zones, 'second', frozen_time))['items']
    expect(formatted.length).to eq(1)
    expect(formatted[0]['title']).to match(/^.* - Second$/)
  end

  it 'filters out everything' do
    formatted = JSON.parse(format_zones_json(zones, 'Nothing will match this', frozen_time))['items']
    expect(formatted.length).to eq(0)
  end

  it 'takes a filter followed by a time' do
    formatted = JSON.parse(format_zones_json(zones, 'second 10', frozen_time))['items']
    expect(formatted[0]['title']).to match(/^20:00 - Second$/)
  end

  it 'takes a time followed by a filter' do
    formatted = JSON.parse(format_zones_json(zones, '10:30 second', frozen_time))['items']
    expect(formatted[0]['title']).to match(/^20:30 - Second$/)
  end

  it 'can have pm at on the short time' do
    formatted = JSON.parse(format_zones_json(zones, '1p first', frozen_time))['items']
    expect(formatted[0]['title']).to match(/^12:00 - First$/)
  end

  it 'can have pm at on the long time' do
    formatted = JSON.parse(format_zones_json(zones, '1:30PM first', frozen_time))['items']
    expect(formatted[0]['title']).to match(/^12:30 - First$/)
  end

  it 'can have multiple filters' do
    formatted = JSON.parse(format_zones_json(zones, 'ir 10 FI', frozen_time))['items']
    expect(formatted.length).to eq(1)
    expect(formatted[0]['title']).to match(/^09:00 - First$/)
  end

  it 'selects the last number as the time value' do
    formatted = JSON.parse(format_zones_json(zones, '10 10', frozen_time))['items']
    expect(formatted.length).to eq(1)
    expect(formatted[0]['title']).to match(/^20:00 - 10$/)
  end
end
