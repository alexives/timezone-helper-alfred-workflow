require_relative '../lib/update_handler.rb'

describe 'handle_update' do
  def get_zones(_file)
    {
      'NA Eastern Time' => -5,
      'NA Central Time' => -6,
      'NA Pacific Time' => -8,
      'EU Central Time' => 1,
      "South Africa Standard Time": 2
    }
  end

  it 'adds a new item to the file' do
    expected = get_zones('')
    expected['A'] = 10
    expect(File).to receive(:write).with('zones.json', expected.to_json)
    handle_update('A, 10')
  end

  it 'removes an item from the file' do
    expected = get_zones('')
    expected.delete('NA Eastern Time')
    expect(File).to receive(:write).with('zones.json', expected.to_json)
    handle_update('NA Eastern Time, 0, true')
  end

  it 'updates an item in the file' do
    expected = get_zones('')
    expected['NA Eastern Time'] = 10
    expect(expected.keys.length).to eq(get_zones('').keys.length)
    expect(File).to receive(:write).with('zones.json', expected.to_json)
    handle_update('NA Eastern Time, 10')
  end
end
