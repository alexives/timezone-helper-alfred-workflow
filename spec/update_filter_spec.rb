require_relative '../lib/update_filter.rb'

describe 'get_new_zone' do
  it 'returns an valid addition' do
    expect(get_new_zone('Test', '-1')[:valid]).to be true
  end

  it 'works with a + in the number' do
    expect(get_new_zone('Test', '+11')[:valid]).to be true
  end

  it 'works with 14 (Line Islands Time)' do
    expect(get_new_zone('Test', '14')[:valid]).to be true
  end

  it 'returns an additon with an autocomplete' do
    expect(get_new_zone('Test', nil)[:valid]).to be false
    expect(get_new_zone('Test', nil)[:autocomplete]).to eq('Test, ')
  end

  it 'returns an error if it is not a valid offset' do
    expect(get_new_zone('Test', 'test')[:valid]).to be false
    expect(get_new_zone('Test', '15')[:valid]).to be false
    expect(get_new_zone('Test', '-13')[:valid]).to be false

    expect(get_new_zone('Test', '-13')[:title]).to eq('ERROR: Add/update zone Test/-13')
    expect(get_new_zone('Test', '-13')[:subtitle]).to eq('Invalid offset, must be an integer between -12 and +14 but is -13')
  end
end

describe 'format_update_json' do
  let(:existing_zones) do
    {
      'NA Eastern Time' => -5,
      'NA Central Time' => -6,
      'NA Pacific Time' => -8,
      'EU Central Time' => 1,
      "South Africa Standard Time": 2
    }
  end

  it 'gives a list with all of the current listings when given no inputs' do
    expect(JSON.parse(format_update_json(existing_zones, ''))['items'].length).to eq(5)
  end

  it 'filters the list by letters and offers to add' do
    expect(JSON.parse(format_update_json(existing_zones, 'Central'))['items'].length).to eq(3)
  end

  it 'gives a valid exact match and no others' do
    expect(JSON.parse(format_update_json(existing_zones, 'NA Eastern Time'))['items'].length).to eq(1)
    expect(JSON.parse(format_update_json(existing_zones, 'NA Eastern Time, -1'))['items'].first['valid']).to be true
  end
end
